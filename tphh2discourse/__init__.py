import os  # for discourse credentials
import time
from datetime import datetime, timedelta
import urllib.request
import json
from pydiscourse import DiscourseClient
import logging
import re
import colorama

from colorama import init
init(autoreset=True)
from colorama import Fore, Back, Style

# "Informationsgegenstand" via http://suche.transparenz.hamburg.de/advanced_search
# zum ersetzen von extras['registerobject_type']
types_texts = {
    'baugenehmigungen': "Baugenehmigungen",
    'baumkataster': "Baumkataster",
    'dienstanweisungen': "Dienstanweisungen",
    'geodaten': "Geodaten",
    'gutachten': "Gutachten und Studien",
    'senatmitteilungen': "Mitteilungen des Senats",
    'ohneveroeffentlichungspflicht': "Ohne gesetzliche Verpflichtung",
    'beschluesse': "Öffentliche Beschlüsse",
    'bauleitplaene': "Öffentliche Pläne",
    'senatpetitum': "Senatsbeschlüsse",
    'vergleichbar': "Sonstige Informationen von öffentlichem Interesse",
    'vertraegeoeffinteresse': "Sonstige Verträge von öffentlichem Interesse",
    'statistiken': "Statistiken und Tätigkeitsberichte",
    'subventionen': "Subventionen und Zuwendungen",
    'messungen': "Umweltmessungen und Erhebungen",
    'staedtischebeteiligungen': "Unternehmensdaten",
    'andereveroeffentlichungspflicht': "Veröffentlichungspflicht außerhalb HmbTG",
    'daseinsvorsorge': "Verträge der Daseinsvorsorge",
    'verwaltungsbelange': "Verwaltungspläne",
    'verwaltungsvorschriften': "Verwaltungsvorschriften"
}

# via http://suche.transparenz.hamburg.de/advanced_search
# zum ersetzen von result['type']
datatypes_texts = {
    'document': "Dokument", 
    'dataset': "Datensatz",
    'app': "Anwendung"
}


def humanbytes(B):
    """Return the given bytes as a human friendly KB, MB, GB, or TB string"""
    # from https://stackoverflow.com/questions/12523586/python-format-size-application-converting-b-to-kb-mb-gb-tb/31631711#31631711

    B = int(B)
    KB = 1024
    MB = KB ** 2 # 1,048,576
    GB = KB ** 3 # 1,073,741,824
    TB = KB ** 4 # 1,099,511,627,776

    if B < KB:
        string = '{0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte')
    elif KB <= B < MB:
        string = '{0:.2f} KB'.format(B/KB)
    elif MB <= B < GB:
        string = '{0:.2f} MB'.format(B/MB)
    elif GB <= B < TB:
        string = '{0:.2f} GB'.format(B/GB)
    elif TB <= B:
        string = '{0:.2f} TB'.format(B/TB)

    return string.replace(".", ",")  # germanize


def get_extra(extras, key):
    """extract a specific key's value from an "extra" dict"""
    for extra in extras:
        if extra['key'] == key:
            return extra['value']


def resource_to_markdown(resource):
    """Create a Markdown list item from a result['resources'][] dict.

    A list element with 3 lines this is."""
    
    """zb
   'resources': [{'mimetype': None,
   'cache_url': None,
   'hash': '197c383e18c8dd0dff8282afb390eb91304035b7eb790ee8402e5209b68d7386',
   'description': '',
   'language': 'de',
   'cache_last_updated': None,
   'url': 'http://daten.transparenz.hamburg.de/Dataport.HmbTG.ZS.Webservice.GetRessource100/GetRessource100.svc/bec9e089-bcdb-4f6e-a558-97dfc0e8b9e2/Oberflaechenwasser.csv',
   'format': 'CSV',
   'state': 'active',
   'created': '2017-03-10T20:12:46.129561',
   'package_id': '0624742b-e0a3-4b0d-aaab-333b84f105ce',
   'mimetype_inner': None,
   'last_modified': None,
   'file_size': '9951',
   'position': 0,
   'revision_id': '1a9ef7c6-6edc-466f-aada-123679c0529d',
   'size': 9951,
   'url_type': None,
   'id': '8f6db9e7-4510-4b9f-ad95-5f8115bb0795',
   'resource_type': None,
   'name': 'Oberflächenwasser'}],"""

   # size kann -1 sein, file_size ist ein string aber richtig...
    link = f"[**{resource['name'].strip()}**]({resource['url']})"
    format = f"Dateiformat: {resource['format']}"
    
    size = resource.get('file_size')
    if size:
        size = f"Dateigröße: {humanbytes(size)}"
    else:
        size = ""

    hash = f"SHA256: `{resource['hash']}`"
    return f"""- {link}
<small>{format}
{size}
{hash}</small>
"""

def registerobject_type_to_text(registerobject_type):
    """make a string from whatever registerobject_type contains

    result kann mehrere Informationsgegenstand(e) haben, komma getrennt ala "'geodaten, bauleitplaene, messungen'"
    """

    if not registerobject_type:
        return "Keine Angabe"

    texts = []
    
    if "," in registerobject_type:
        types = registerobject_type.split(", ")
    else:
        types = (registerobject_type,)

    for type in types:
        text = types_texts.get(type)
        
        if text:
            texts.append(text)

    if texts:
        return ", ".join(texts)
    else:
        return "Keine Angabe"

    

def result_to_markdown(result):
    """Create a Markdown text for a result from the API.

    result: something from e.g. http://suche.transparenz.hamburg.de/api/3/action/package_show?id=
    """
    # omg
    # WORKSFORME
    
    href = f"http://suche.transparenz.hamburg.de/dataset/{result['name']}"
    # newlines können vorkommen, müssen für discourse entfernt werden. siehe http://suche.transparenz.hamburg.de/api/3/action/package_show?id=neubewertung-des-gesamtkomplexes-lager-produktion-buero-als-1-brandabschnittnutzungsaenderung-e
    name = result['name'].replace('\n', ' ')
    href = f"http://suche.transparenz.hamburg.de/dataset/{name}"
    title = " ".join(result['title'].split("\n"))

    # linked title, url and link to mailto: like in tphh
    body = f"""
## [{title}]({href})

{href}

[![](/uploads/default/original/1X/2a675ec3fd98449c345a4825fc3c115a206550f2.png) Rückmeldung via Transparenzportal](mailto:Transparenzportal@kb.hamburg.de?subject=Anfrage%20zum%20Registereintrag:%20{result['name']}&amp;body=Sehr%20geehrte%20Damen%20und%20Herren,%0A%0Azu%20folgendem%20Eintrag%20habe%20ich%20eine%20Anmerkung/Frage:%0A%0ATitel:%20{urllib.request.pathname2url(result['title'])}%0A%0ADokument-ID:%20{result['name']}%0A%0AURL:%20{href}%0A%0AMein%20Kommentar:%0A%0AViele%20Gr%C3%BC%C3%9Fe,%0A)

"""

    # (potentially long) description
    if result['notes']:
        body += f"### Beschreibung\n{result['notes']}\n\n"

    body += f"""
### Metadaten

|||
|-|-|
|Veröffentlichende Stelle|{result['author']}|
|Datentyp|{datatypes_texts[result['type']]}|
|Informationsgegenstand|{registerobject_type_to_text(get_extra(result['extras'], "registerobject_type"))}|
|Kategorie|{"<br />".join([group['title'] for group in result['groups']])}|
|Schlagwörter:|{", ".join([tag['name'] for tag in result['tags']])}|\n"""

    # references, IDs, identifiers
    body += f"""|`metadata_original_id`|`{get_extra(result['extras'], "metadata_original_id")}`|\n"""
    if get_extra(result['extras'], "metadata_zs_original_id"):
        body += f"""|`metadata_zs_original_id`|`{get_extra(result['extras'], "metadata_zs_original_id")}`|\n"""
    if get_extra(result['extras'], "file_reference_digital"):
        body += f"""|`file_reference_digital`|`{get_extra(result['extras'], "file_reference_digital")}`|\n"""
    if get_extra(result['extras'], "source"):
        body += f"""|`source`|`{get_extra(result['extras'], "source")}`|\n"""
    if get_extra(result['extras'], "harvest_source_title"):
        body += f"""|`harvest_source_title`|`{get_extra(result['extras'], "harvest_source_title")}`|\n"""

    # temporal
    if get_extra(result['extras'], "temporal_coverage_from"):
        body += f"""|`temporal_coverage_from`|{get_extra(result['extras'], "temporal_coverage_from").replace("T", " ")}|\n"""
    if get_extra(result['extras'], "temporal_coverage_to"):
        body += f"""|`temporal_coverage_to`|{get_extra(result['extras'], "temporal_coverage_to").replace("T", " ")}|\n"""
    if get_extra(result['extras'], "publishing_date"):
        body += f"""|`publishing_date`|{get_extra(result['extras'], "publishing_date").replace("T", " ")}|\n"""
    if get_extra(result['extras'], "exact_publishing_date"):
        body += f"""|`exact_publishing_date`|{get_extra(result['extras'], "exact_publishing_date").replace("T", " ")}|\n"""
    if result.get('metadata_created'):
        body += f"""|`metadata_created`|{result['metadata_created'].replace("T", " ")}|\n"""
    if result.get('metadata_modified'):
        body += f"""|`metadata_modified`|{result['metadata_modified'].replace("T", " ")}|\n"""
    if get_extra(result['extras'], "offline_date"):
        body += f"""|`offline_date`|{get_extra(result['extras'], "offline_date").replace("T", " ")}|\n"""

    # metadata in other formats
    body += f"""|Gesamte Metadaten|[JSON](http://suche.transparenz.hamburg.de/api/3/action/package_show?id={result['name']}), [N3]({href}.n3), [RDF]({href}.rdf), [TTL]({href}.ttl), [XML]({href}.xml), [JSONLD]({href}.jsonld)|

"""

    # license
    body += f"""
||||
|-|-|-|
|Lizenz:|[{result['license_title']}](https://www.govdata.de/dl-de/by-2-0)|
|Namensnennung:|Freie und Hansestadt Hamburg, {result['author']}|
"""

    # resources
    body += f"\n\n### Ressourcen ({len(result['resources'])})\n"
    body += "\n\n".join([resource_to_markdown(resource) for resource in result['resources']])

    return body


def post_result(discourse_client, result):
    """Process a api search result and post it to the forum."""

    body = result_to_markdown(result)

    # TODO refactor this into a function
    title = result['name']  # + "_hannestest"

    tags = [tag['name'] for tag in result['tags'] if not re.match("(X[IVX]+|2\d)-\d+", tag['name'])]
    tags = set(tags)  # let's make sure everything is unique
    tags.add(result['author'])  # veröffentlichende stelle
    tags.add(registerobject_type_to_text(get_extra(result['extras'], "registerobject_type")))  # informationsgegenstand
    tags.union([group['title'] for group in result['groups']])  # kategorie

    logging.debug(f"""Tags: {", ".join(tags)}""")

    try:
        logging.info(Style.DIM + f"FORUM: Trying to post {title}...")
        post = discourse_client.create_post(
            content=body,
            category_id=5,  # FIXME hardcoded category id!
            title=title,
            tags=tags
        )
        logging.info(Style.BRIGHT + Fore.GREEN + f"FORUM: Successfully posted {title}...")
    except Exception as e:
        if str(e) == "Titel wird bereits verwendet":
            # this is fine.
            logging.warning(Fore.YELLOW + f"FORUM: Could not post {result['name']}: {str(e)}")
            time.sleep(1.23)
        else:
            raise e

        
def discourseClient():
    """Create client object, takes credentials from system env!"""

    username = os.environ['DiscourseUsername']
    api_key = os.environ['DiscourseAPIKey']

    client = DiscourseClient(
            'https://community.codeforhamburg.org',
            api_username=username,
            api_key=api_key
    )

    logging.info(Fore.GREEN + "FORUM: Discourse client ready...")
    
    return client
