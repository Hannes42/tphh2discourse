from datetime import datetime, timedelta
import logging
import feedparser
import time

from tphh2discourse import *

logging.basicConfig(
    format="%(asctime)s.%(msecs)03d\t%(levelname)s\t%(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
)

# DONE via shell TODO können wir für diesen INITIALEN import auch alle alten posts vorher löschen via API?

last_known_timestamp = datetime.now() - timedelta(99999)  # FIXME remove the faking
todo = True
page = 1  # page to start at, api starts counting at 1, not 0
errors = 0

client = discourseClient()

while todo:
    tphh_feed_url = f"http://suche.transparenz.hamburg.de/feeds/advanced.atom?sort=publishing_date+desc&page={page}"
    logging.info(f"FEED: Fetching {tphh_feed_url}")

    feed = feedparser.parse(tphh_feed_url)

    entries = feed['entries']

    if not entries:
        logging.info("FEED: Found no entries in feed, stopping...")
        break

    for entry in entries:
        logging.debug(entry)
        entry_timestamp = datetime.fromtimestamp(time.mktime(entry['published_parsed']))
        
        if entry_timestamp > last_known_timestamp:
            logging.info(f"FEED: Found entry from {entry_timestamp}")
            post_entry(client, entry)
            time.sleep(1.3)
        else:
            todo = False
            logging.info(f"FEED: Reached newer timestamp: {entry_timestamp} > {last_known_timestamp}, exiting...")
            last_known_timestamp = entry_timestamp
            break

    time.sleep(1)

    page += 1

    if errors > 5:
        break
    
    #break
# TODO store last_timestamp
