- Auf Server clonen
- `# apt install python3-pip`
- `$ pip3 install --user pydiscourse`
- Cronjob anlegen (dafür sollte besser in Datei geloggt werden) oder einfach:
```
while sleep 60
do
	echo "---------- Trying to find new results ----------"
	DiscourseUsername=USERNAME DiscourseAPIKey=APIKEY python3 ~/tphh2discourse/letzteXmin_api.py
done
```