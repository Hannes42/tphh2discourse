from datetime import datetime, timedelta
import logging
import time
import os
from tphh2discourse import *
import urllib.request
from urllib.error import URLError  # URLError is the parent error class
import colorama

from colorama import init
init(autoreset=True)
from colorama import Fore, Back, Style

logging.basicConfig(
    format="%(asctime)s.%(msecs)03d\t%(levelname)s\t%(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
)

logging.info(Fore.GREEN + f"SCRIPT: Hello! It's {datetime.utcnow()} and I am checking for new posts!")

try:
    with open(os.path.expanduser("~/last_known_timestamp")) as f:
        last_known_timestamp = datetime.strptime(f.read().strip(), '%Y-%m-%d %H:%M:%S')
    logging.info(Fore.GREEN + f"SCRIPT: Loaded last known timestamp from file: {last_known_timestamp}")
except FileNotFoundError:
    logging.warning(Style.BRIGHT + Fore.RED + f"SCRIPT: No previously stored last known timestamp found.")
    minutes = 9999999  # make sure we start at the very beginning of time
    last_known_timestamp = datetime.utcnow() - timedelta(minutes=minutes)
    logging.info(Fore.YELLOW + f"SCRIPT: Using a timestamp from {minutes} minutes ago: {last_known_timestamp} as a reasonable start")

newest_timestamp_seen = last_known_timestamp

todo = True
start = 0
rows = 20  # per page
errors = 0
max_errors = 5

client = discourseClient()

while todo:
    # TODO können wir in der suche direkt mit einem bestimmten datum starten oder enden??
    search_url = f"http://suche.transparenz.hamburg.de/api/3/action/package_search?sort=exact_publishing_date+desc&rows={rows}&start={start}"  # exact_pub date!! to mirror suche.tphh
    logging.info(Style.DIM + f"API: Fetching {search_url}")

    req = urllib.request.Request(
        search_url, data=None,
        headers={'User-Agent': 'community.codeforhamburg.org'}
    )

    try:
        with urllib.request.urlopen(req) as req:
            response = req.read()
            response = json.loads(response)
            result = response['result']
            #logging.debug(f"API result: {result}")
    except (URLError, ConnectionResetError) as e:
        logging.warning(Style.BRIGHT + Fore.RED + f"API: {str(e)}")
        errors += 1

        if errors > max_errors:
            logging.critical(Style.BRIGHT + Fore.RED + f"API: More than {errors} errors, quitting...")
            break
        else:
            continue  # = try again with another iteration using the same "page"


    results = response['result']['results']
    if not results:
        logging.info(Style.BRIGHT + Fore.RED + "API: Got no results from API, stopping...")
        break

    for result in results:
        logging.debug(Style.DIM + f"API result: {result}")
        logging.info(f"API: {result['name']}")

        # ganz am anfang befinden sich einige testeinträge, was auch immer das soll...
        if "forward-reference-" in result['name'] or result['name'] in [
            "allrishamburgmitte",
            "bacomsourcezsfile",
            "workflowsourcezs",
            "allriseimsbuettel",
            "statistiknord",
            "allrisharburg",
            "allriswandsbek",
            "allrisbergedorf",
            "allrishamburgnord",
            "allrishaltona",
            "imissourcezs",
            "bacomsourcezsnew",
            "dokratessource",
            "hmdk",
            "inezsourcezs",
        ]:
            # ffs
            logging.info(Style.BRIGHT + Fore.YELLOW + f"Skipping known invalid entry {result['name']}...")
            continue

        # TODO we need to look at updated_parsed, not published_parsed or a old entry being updated would stop the loop...?
        exact_publishing_date = get_extra(result['extras'], 'exact_publishing_date')
        result_timestamp = datetime.strptime(exact_publishing_date, "%Y-%m-%dT%H:%M:%S")
        if not exact_publishing_date:
            logging.warning(Style.BRIGHT + Fore.RED + f"API result: No exact_publishing_date for {result['name']}!")

        # a greater date is more recent than a lesser date
        # so if we get a greater date than previously known, it is a new entry
        if result_timestamp >= last_known_timestamp:
            logging.info(f"API: Found potentially new result from {result_timestamp}")
            try:
                post_result(client, result)
            except Exception as e:
                logging.warning(result)
                raise(e)
            time.sleep(1.3)  # letting the APIs rest
        else:
            logging.info(Style.BRIGHT + Fore.GREEN + f"API: Newest result {result_timestamp} is older than the last known {last_known_timestamp}, exiting...")

            todo = False  # to kill the outermost loop
            break

        if result_timestamp > last_known_timestamp and result_timestamp > newest_timestamp_seen:
            newest_timestamp_seen = result_timestamp
            logging.info(f"newest_timestamp_seen: {newest_timestamp_seen}")

    start += rows
    # TODO hier kann es bei race condition "tphh hat neue einträge während diese schleife läuft" zu überspringungen kommen...

last_known_timestamp = newest_timestamp_seen

with open(os.path.expanduser("~/last_known_timestamp"), "w") as f:
    f.write(last_known_timestamp.strftime('%Y-%m-%d %H:%M:%S'))
    logging.info(Fore.GREEN + f"SCRIPT: Saved last known timestamp to file: {last_known_timestamp}")


# todo can we ask the API for entries newer than XXXX?
